﻿using OpenTK;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace SoundSignatureLogger
{
    class RealtimeAudio
    {
        #region Fields

        AudioContext audio_context;
        AudioCapture audio_capture;

        int src;
        public short[] buffer = new short[512];
        const byte SampleToByte = 2;
        float numericUpDown_PlaybackGain = 100f;
        int numericUpDown_Frequency = 5512;
        int numericUpDown_BufferLength = 50;
        IList<string> recorders;
        public Timer timer_GetSamples;

        #endregion

        #region Constructors

        public RealtimeAudio()
        {

            // Add available capture devices to the combo box.
            recorders = AudioCapture.AvailableDevices;
        }

        #endregion

        #region Events

        public void button_Start_Click()
        {
            if (audio_capture == null || !audio_capture.IsRunning)
            {
                this.StartRecording();
            }
            else
            {
                this.StopRecording();
            }
        }

        public void timer_UpdateSamples_Tick(object sender, EventArgs e)
        {
            this.UpdateSamples();
        }

        public void Parrot_FormClosing()
        {
            this.StopRecording();
        }

        #endregion

        #region Private Members

        void StartRecording()
        {
            try
            {
                audio_context = new AudioContext();
            }
            catch (AudioException ae)
            {Console.WriteLine("Fatal: Cannot continue without a playback device.\nException caught when opening playback device.\n" + ae.Message);
            }

            AL.Listener(ALListenerf.Gain, (float)numericUpDown_PlaybackGain);
            src = AL.GenSource();

            int sampling_rate = (int)numericUpDown_Frequency;
            double buffer_length_ms = (double)numericUpDown_BufferLength;
            int buffer_length_samples = (int)((double)numericUpDown_BufferLength * sampling_rate * 0.001 / BlittableValueType.StrideOf(buffer));

            try
            {
                audio_capture = new AudioCapture(recorders[0],
                    sampling_rate, ALFormat.Mono16, buffer_length_samples);
            }
            catch (AudioDeviceException ade)
            {
                Console.WriteLine("Exception caught when opening recording device.\n" + ade.Message);
                audio_capture = null;
            }

            if (audio_capture == null)
                return;

            audio_capture.Start();

            timer_GetSamples.Elapsed += timer_UpdateSamples_Tick;
            timer_GetSamples.Interval = (int)(buffer_length_ms / 2 + 0.5);   // Tick when half the buffer is full.
            timer_GetSamples.Start();
        }

        void StopRecording()
        {
            timer_GetSamples.Stop();

            if (audio_capture != null)
            {
                audio_capture.Stop();
                audio_capture.Dispose();
                audio_capture = null;
            }

            if (audio_context != null)
            {
                int r;
                AL.GetSource(src, ALGetSourcei.BuffersQueued, out r);
                ClearBuffers(r);

                AL.DeleteSource(src);

                audio_context.Dispose();
                audio_context = null;
            }
        }

        void UpdateSamples()
        {
            if (audio_capture == null)
                return;

            int available_samples = audio_capture.AvailableSamples;

            if (available_samples * SampleToByte > buffer.Length * BlittableValueType.StrideOf(buffer))
            {
                buffer = new short[MathHelper.NextPowerOfTwo(
                    (int)(available_samples * SampleToByte / (double)BlittableValueType.StrideOf(buffer) + 0.5))];
            }

            if (available_samples > 0)
            {
                audio_capture.ReadSamples(buffer, available_samples);

                int buf = AL.GenBuffer();
                AL.BufferData(buf, ALFormat.Mono16, buffer, (int)(available_samples * BlittableValueType.StrideOf(buffer)), audio_capture.SampleFrequency);
                AL.SourceQueueBuffer(src, buf);

                //label_SamplesConsumed.Text = "Samples consumed: " + available_samples;

                if (AL.GetSourceState(src) != ALSourceState.Playing)
                    AL.SourcePlay(src);
            }

            ClearBuffers(0);
        }

        void ClearBuffers(int input)
        {
            if (audio_context == null || audio_context == null)
                return;

            int[] freedbuffers;
            if (input == 0)
            {
                int BuffersProcessed;
                AL.GetSource(src, ALGetSourcei.BuffersProcessed, out BuffersProcessed);
                if (BuffersProcessed == 0)
                    return;
                freedbuffers = AL.SourceUnqueueBuffers(src, BuffersProcessed);
            }
            else
            {
                freedbuffers = AL.SourceUnqueueBuffers(src, input);
            }
            AL.DeleteBuffers(freedbuffers);
        }


        #endregion
    }
}
