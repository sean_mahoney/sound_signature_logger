﻿using SoundFingerprinting;
using SoundFingerprinting.Audio;
using SoundFingerprinting.Builder;
using SoundFingerprinting.Command;
using SoundFingerprinting.Configuration;
using SoundFingerprinting.DAO.Data;
using SoundFingerprinting.Data;
using SoundFingerprinting.InMemory;
using SoundFingerprinting.Strides;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SoundSignatureLogger
{
    class SoundFingerprintClass
    {
        private readonly InMemoryModelService modelService = new InMemoryModelService(); // store fingerprints in RAM
        private readonly SoundFingerprintingAudioService audioService = new SoundFingerprintingAudioService(); // default audio library

        public async Task StoreForLaterRetrieval(string pathToAudioFile, string title, string artist)
        {
            var track = new TrackInfo(pathToAudioFile, title, artist);

            var fingerprintConfig = new HighPrecisionFingerprintConfiguration();
            //fingerprintConfig.FingerprintLengthInSeconds = .1f;

            // create fingerprints
            var hashedFingerprints = await FingerprintCommandBuilder.Instance
                                        .BuildFingerprintCommand()
                                        .From(pathToAudioFile)
                                        .WithFingerprintConfig(fingerprintConfig)
                                        .UsingServices(audioService)
                                        .Hash();

            // store hashes in the database for later retrieval
            modelService.Insert(track, hashedFingerprints);
        }

        public async Task<TrackData> GetBestMatchForAudio(string queryAudioFile)
        {
            int secondsToAnalyze = 10; // number of seconds to analyze from query file
            int startAtSecond = 0; // start at the begining

            var queryConfig = new HighPrecisionQueryConfiguration();
            //queryConfig.FingerprintConfiguration.FingerprintLengthInSeconds = .25f;
            //queryConfig.PermittedGap = .1;
            //Console.WriteLine(queryConfig.PermittedGap);

            // query the underlying database for similar audio sub-fingerprints
            var queryResult = await QueryCommandBuilder.Instance.BuildQueryCommand()
                                                 .From(queryAudioFile, secondsToAnalyze, startAtSecond)
                                                 .WithQueryConfig(queryConfig)
                                                 .UsingServices(modelService, audioService)
                                                 .Query();

            return queryResult.BestMatch.Track;
        }

        public async Task<TrackData> GetRealtimeMatchForAudio(short[] buffer)
        {
            int secondsToAnalyze = 10; // number of seconds to analyze from query file
            int startAtSecond = 0; // start at the begining
            int waitTime = 3000;
            var cancellationTokenSource = new CancellationTokenSource(waitTime);
            var list = new List<Hashes>();
            var audio = new AudioSamples(buffer,0,5512);

            var queryConfig = new HighPrecisionQueryConfiguration();
            //queryConfig.FingerprintConfiguration.FingerprintLengthInSeconds = .25f;
            //queryConfig.PermittedGap = .1;
            //Console.WriteLine(queryConfig.PermittedGap);

            // query the underlying database for similar audio sub-fingerprints
            var queryResult = await QueryCommandBuilder.Instance.BuildRealtimeQueryCommand()
                .From(buffer)
                .WithRealtimeQueryConfig(config =>
                {
                    config.QueryFingerprintsCallback += timedHashes => list.Add(timedHashes);
                    config.Stride = new IncrementalStaticStride(512);
                    return config;
                })
                .UsingServices(modelService)
                .Query(cancellationTokenSource.Token);

            return queryResult.BestMatch.Track;
        }
    }
}
