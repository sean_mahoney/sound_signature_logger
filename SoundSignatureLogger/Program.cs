﻿using OpenTK;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SoundSignatureLogger
{
    class Program
    {
        short[] _buffer = new short[512];
        const byte SampleToByte = 2;

        static async Task Main(string[] args)
        {

            SoundFingerprintClass fingerprint = new SoundFingerprintClass();
            await fingerprint.StoreForLaterRetrieval("Audio\\Finger snap 3.wav", "Finger Snap", "Other Sounds");
            await fingerprint.StoreForLaterRetrieval("Audio\\chopin_short.wav", "song", "chopin");

            var ra = new RealtimeAudio();
            ra.button_Start_Click();
            while (true)
            {
                // TODO change from short buffer to float buffer.
                // TODO change buffer size to 10240 samples
                // TODO Build BlockingCollection<AudioSamples> for Realtime query 
                //      https://gist.github.com/AddictedCS/8ce2ec2614f1f7ef70b68dbf6b38e331 -
                //      https://github.com/AddictedCS/soundfingerprinting/blob/121af8f801cbf51d28f3fcaca305c8257b6aff6e/src/SoundFingerprinting.Tests/Unit/Query/RealtimeQueryCommandTest.cs
                //      https://github.com/AddictedCS/soundfingerprinting/issues/144
                // TODO Build and test realtime query
            }
            //Console.WriteLine(fingerprint.GetBestMatchForAudio("Audio\\Finger snap 2.wav").Result.Artist);
        }

    }
}
